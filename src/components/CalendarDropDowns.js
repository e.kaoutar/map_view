import React, { Component } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css'
import ReactDOM from 'react-dom'
import Calendar from './media/calendar.svg'
import poppins from 'typeface-poppins'
import arrowdown from './media/arrowdown.svg'

export class CalendarDropDowns extends Component {

    //style functions

    getContainerStyle = () => {
        const BackContainer = "rgba(148,148,168,0,08)";
        return{
            width: '1000px',
            marginLeft:'150px',
            marginTop:'50px',
            height: '78px',
            border: 'solid 1px #dadce9',
            background: '#ffffff',
            boxShadow: `1px 3px 1px ${BackContainer}`,
            borderRadius:'2px'
        }
    }

    getCalendarButtonsContainer = () => {
        
        return{
            width: '362px',
            height: '50px',
            marginLeft:'10px',
            marginTop:'15px',
            
        }
        
    }

    getStyleDropDownDeparture = () => {
        const BackButtonsCalendar = "rgba(208,217,221,0,5)";
        return{
            width:'180px',
            height:'48px',
            border: 'solid 1px #dadce9',
            background: '#ffffff',
            boxShadow: `0px 10px 7px 1px ${BackButtonsCalendar}`,
            borderRadius:'2px',
            fontFamily: poppins
        }
        
    }

    render() {
        return (
            <div style={this.getContainerStyle()}>
                <div style={this.getCalendarButtonsContainer()}>
                    <div class="btn-group">
                        <button type="button" className="btn btn-light" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style={this.getStyleDropDownDeparture()}><img src={Calendar}/> Departure &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src={arrowdown}/></button>
                        <button type="button" className="btn btn-light" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style={this.getStyleDropDownDeparture()}><img src={Calendar}/> Return &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <img src={arrowdown}/></button>
                    </div>
                </div>
            </div>
        )
    }
}

export default CalendarDropDowns
