import React, { Component } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css'
import ReactDOM from 'react-dom'
import Calendar from './media/calendar.svg'
import poppins from 'typeface-poppins'
import arrowdown from './media/arrowdown.svg'
import palm from './media/palm.svg'
import planet from './media/planet.svg'
import money from './media/money.svg'
import closeshape from './media/closeshape.svg'

export class Topbar extends Component {

    //style functions

    getContainerStyle = () => {
        const BackContainer = "rgba(148,148,168,0,08)";
        return{
            width: '1045px',
            marginLeft:'125px',
            marginTop:'50px',
            height: '78px',
            border: 'solid 1px #dadce9',
            background: '#ffffff',
            boxShadow: `1px 3px 1px ${BackContainer}`,
            borderRadius:'2px'
        }
    }

    getCalendarButtonsContainer = () => {
        
        return{
            width: '362px',
            height: '50px',
            marginLeft:'10px',
            marginTop:'15px',
            
        }
        
    }

    getStyleDropDownButtons = () => {
        const BackButtonsCalendar = "rgba(208,217,221,0,5)";
        return{
            width:'160px',
            height:'48px',
            border: 'solid 1px #dadce9',
            color: '#2e333b',
            background: '#ffffff',
            boxShadow: `0px 10px 7px 1px ${BackButtonsCalendar}`,
            borderRadius:'2px',
            fontFamily: poppins,
            fontSize: '14px',
            fontWeight: 400,
            lineHeight: '21px'
        }
        
    }

    getStyleCloseMap = () =>{
        const BackButtonsCalendar = "rgba(208,217,221,0,5)";
        return{
            width:'160px',
            height:'48px',
            border: 'solid 1px #dadce9',
            background: '#ffffff',
            boxShadow: `0px 10px 7px 1px ${BackButtonsCalendar}`,
            borderRadius:'2px',
            fontFamily: poppins,
            fontSize: '12px',
            lineHeight: '18px',
            textTransform: 'uppercase',
            letterSpacing: '1px'
        }
    }

    render() {
        return (
            <div style={this.getContainerStyle()}>
                <div style={this.getCalendarButtonsContainer()}>
                
                    <div class="btn-group">
                        <button type="button" className="btn btn-light" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style={this.getStyleDropDownButtons()}><img src={Calendar}/> Departure &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src={arrowdown}/></button>
                        <button type="button" className="btn btn-light" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style={this.getStyleDropDownButtons()}><img src={Calendar}/> Return &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <img src={arrowdown}/></button>
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <button type="button" className="btn btn-light" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style={this.getStyleDropDownButtons()}><img src={palm}/> Trip Type &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <img src={arrowdown}/></button>
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <button type="button" className="btn btn-light" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style={this.getStyleDropDownButtons()}><img src={planet}/> Continents &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <img src={arrowdown}/></button>
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <button type="button" className="btn btn-light" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style={this.getStyleDropDownButtons()}><img src={money}/> Price &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <img src={arrowdown}/></button>
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <button type="button" className="btn btn-light"style={this.getStyleCloseMap()}><img src={closeshape}/>  CLOSE MAP VIEW </button>
                    </div>
                                       
                </div>
            </div>
        )
    }
}

export default Topbar
