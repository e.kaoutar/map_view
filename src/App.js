import React from 'react';
import {Component} from 'react';
import Topbar from './components/Topbar';
import './App.css';

class App extends Component{

  //state definition
  state={
    reservation_params:[
      {
        departure:'testDep',
        return:'testRet',
        price:'testPrice',
        trip_type:'testTrip',
        country:'testCountry'
      }
    ]
  }

  render(){
    return (
      <Topbar/>
    );
  }
  
}

export default App;
